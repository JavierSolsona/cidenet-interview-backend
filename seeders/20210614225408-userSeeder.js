'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Users', [{
      firstName: 'John',
      lastName: 'Doe',
      secondLastName: 'Jane',
      country: 'CO',
      identificationType: 'C',
      identification: '12345',
      email: 'john.doe@cidenet.com.co',
      registerDate: new Date(),
      area: '0',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      firstName: 'Mary',
      lastName: 'Parker',
      secondLastName: 'Lane',
      country: 'US',
      identificationType: 'P',
      identification: '67890',
      email: 'mary.parker@cidenet.com.us',
      registerDate: new Date(),
      area: '1',
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});
  }
};
