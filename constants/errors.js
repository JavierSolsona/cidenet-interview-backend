exports.INTERNAL_SERVER_ERROR = 'Internal Server Error';
exports.BAD_REQUEST = 'Bad request';
exports.ID_INT = 'ID must be integer greater than 1';
exports.OBJECT_NOT_FOUND = 'Object not found';
exports.OBJECT_NOT_DELETED = 'Object not deleted';