'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      firstName: {
        type: Sequelize.STRING(20),
        allowNull: false
      },
      lastName: {
        type: Sequelize.STRING(20),
        allowNull: false
      },
      othersName: {
        type: Sequelize.STRING(50)
      },
      secondLastName: {
        type: Sequelize.STRING(20),
        allowNull: false
      },
      country: {
        type: Sequelize.ENUM('CO', 'US'),
        allowNull: false
      },
      identificationType: {
        type: Sequelize.ENUM('C', 'E', 'P', 'PE'),
        allowNull: false
      },
      identification: {
        type: Sequelize.STRING(20),
        allowNull: false,
        unique: true
      },
      email: {
        type: Sequelize.STRING(300),
        allowNull: false,
        unique: true
      },
      registerDate: {
        type: Sequelize.DATEONLY,
        allowNull: false
      },
      area: {
        type: Sequelize.ENUM('0', '1', '2', '3', '4', '5', '6', '7', '8'),
        allowNull: false,
      },
      status: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Users');
  }
};