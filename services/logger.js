const winston = require('winston');

dateFormat = () => {
  return new Date( Date.now()).toUTCString();
}
  
class Logger {
  constructor(route) {
    this.route = route;
    
    const logger = winston.createLogger({
      transports: [
        new winston.transports.Console(),
        new winston.transports.File({
          filename: `./logs/${route}.log`
        })
      ],
      format: winston.format.printf((info) => {
        let message = `${dateFormat()} | ${info.level.toUpperCase()} | ${route}.log | ${info.message} | `;
        
        return message
      })
    });
    
    this.logger = logger;
  }
  
  async info(message) {
    this.logger.log('info', message);
  }
  
  async error(message) {
    this.logger.log('error', message);
  }
}

module.exports = Logger;