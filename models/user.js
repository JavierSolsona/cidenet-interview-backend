'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  User.init({
    firstName: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    lastName: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    othersName: {
      type: DataTypes.STRING(50)
    },
    secondLastName: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    country: {
      type: DataTypes.ENUM('CO', 'US'),
      allowNull: false
    },
    identificationType: {
      type: DataTypes.ENUM('C', 'E', 'P', 'PE'),
      allowNull: false
    },
    identification: {
      type: DataTypes.STRING(20),
      allowNull: false,
      unique: true
    },
    email: {
      type: DataTypes.STRING(300),
      allowNull: false,
      unique: true
    },
    registerDate: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    area: {
      type: DataTypes.ENUM('0', '1', '2', '3', '4', '5', '6', '7', '8'),
      allowNull: false,
    },
    status: {
      type: DataTypes.BOOLEAN
    },
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};