const models  = require('../models');
const fs = require('fs');
const { body, param, validationResult } = require('express-validator');

const {
  INTERNAL_SERVER_ERROR,
  ID_INT,
  BAD_REQUEST,
  OBJECT_NOT_FOUND,
  OBJECT_NOT_DELETED
} = require('../constants/errors');
const { OBJECT_DELETED } = require('../constants/success');
const Logger = require('../services/logger');


const logger = new Logger('users');

exports.validate = (method) => {
  switch (method) {
    case 'urlParameter': {
      return [
        param('userId').isInt({min: 1}).withMessage(ID_INT)
      ]
    }
  }
}

exports.index = (req, res) => {
    models.User.findAll()
        .then((result) => {
            logger.info('Get users index');
            return res.status(200).send(result);
        }).catch((err) => {
            logger.error('Error in get users index');
            return res.status(500).send({errors: [], message: INTERNAL_SERVER_ERROR});
        });
};

exports.getById = (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      logger.error('Validation error in delete users');
      return res.status(400).send({errors: errors.array(), message: BAD_REQUEST});
    }

    models.User.findByPk(req.params.userId)
        .then((result) => {
            if (result) {
              logger.info(`Get user by id: ${req.params.userId}`);
              return res.status(200).send(result);
            }else {
              logger.error('Error in get user by id');
              return res.status(417).send({errors: [], message: OBJECT_NOT_FOUND});
            }
        }).catch((err) => {
          logger.error('Error in get user by id');
          return res.status(500).send({errors: [], message: INTERNAL_SERVER_ERROR});
        });
};

exports.delete = (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      logger.error('Validation error in delete users');
      return res.status(400).send({errors: errors.array(), message: BAD_REQUEST});
    }

    models.User.findByPk(req.params.userId)
        .then((result) => {
          if (result) {
            models.User.destroy({where: { id: req.params.userId } })
              .then((deleted) => {
                  if (deleted === 0) {
                    logger.error('Error in delete users');
                    return res.status(417).send({errors: [], message: OBJECT_NOT_DELETED});
                  } else {
                    logger.info(`User deleted with id: ${req.params.userId}`);
                    return res.status(200).send({message: OBJECT_DELETED});
                  }
              }).catch((err) => {
                logger.error('Error in delete users');
                return res.status(500).send({errors: [], message: INTERNAL_SERVER_ERROR});
              });
          }else {
            logger.error('Error in delete users');
            return res.status(417).send({errors: [], message: OBJECT_NOT_FOUND});
          }
        }).catch((err) => {
            logger.error('Error in delete users');
            return res.status(500).send({errors: [], message: INTERNAL_SERVER_ERROR});
        });
};