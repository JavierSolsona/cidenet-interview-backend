var express = require('express');
var router = express.Router();
const UsersController = require('../controllers/users-controller');

router.get('/', UsersController.index);
router.get('/:userId', UsersController.validate('urlParameter'), UsersController.getById);
router.delete('/:userId', UsersController.validate('urlParameter'), UsersController.delete);

module.exports = router;
